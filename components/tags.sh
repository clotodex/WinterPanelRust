#!/bin/bash

{
	herbstclient --idle "^tag"
} | {
	while true; do
		IFS=$'\t' read -ra cmd #wait for input
		str=""
		IFS=$'\t' read -ra tags <<< "$(herbstclient tag_status)" #normally do for every monitor
		# draw tags
		for i in "${tags[@]}" ; do
			case ${i:0:1} in
				'#')
					str+="^bg($selbg)^fg($selfg)"
					;;
				'+')
					str+="^bg(#9CA668)^fg(#141414)"
					;;
				':')
					str+="^bg()^fg(#ffffff)"
					;;
				'!')
					str+="^bg(#FF0675)^fg(#141414)"
					;;
				*)
					str+="^bg()^fg(#ababab)"
					;;
			esac
				# clickable tags if using SVN dzen
				str+="^ca(1,\"${herbstclient_command[@]:-herbstclient}\" "
				str+="focus_monitor \"$monitor\" && "
				str+="\"${herbstclient_command[@]:-herbstclient}\" "
				str+="use \"${i:1}\") ${i:1} ^ca()"
		done
		herbstclient emit_hook "[wp]" "event" "tags" "$str"
	done
}
