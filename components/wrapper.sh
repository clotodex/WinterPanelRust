#!/bin/bash

#TODO
# protocol wrapper so the underlying program just has to output the dzen string

declare -a comps
comps=( "battery" "time_and_date" )
{
	herbstclient --last-arg --idle "\[scheduler\]"
} | {
	while true; do
		IFS=$'\t' read -ra cmd ;
		for comp in "${comps[@]}"
		do
			out="$(./components/"$comp".sh)"
			herbstclient emit_hook "[wp]" "event" "$comp" "$out"
		done
	done
}
