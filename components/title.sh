#!/bin/bash
{
	herbstclient --last-arg --idle "(focus_changed|window_title_changed)"
} | {
	while true; do
		IFS=$'\t' read -ra cmd ;
		herbstclient emit_hook "[wp]" "event" "title" "$cmd"
	done
}
