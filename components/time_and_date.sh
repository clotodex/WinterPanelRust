#!/bin/bash

d="$(date +$'^fg(#efefef)%H:%M:%S^fg()|^fg(#909090), %Y-%m-^fg(#efefef)%d')"
herbstclient emit_hook "[wp]" "event" "date" "${d#*|}"
herbstclient emit_hook "[wp]" "event" "time" "${d%|*}"
