#!/bin/bash

powerfull=$(cat /sys/class/power_supply/BAT0/energy_full)
powernow=$(cat /sys/class/power_supply/BAT0/energy_now)
power="$(( powernow * 100 / powerfull ))"

power_color="red"
power_fg_color="white"
if [ $power -ge 10 ]; then power_color="orange"; fi
if [ $power -ge 25 ]; then power_color="yellow";power_fg_color="black"; fi
if [ $power -ge 50 ]; then power_color="green"; power_fg_color="black"; fi
power="$power%"
power="^bg($power_color)^fg($power_fg_color)$power^bg()"

echo "$power"
