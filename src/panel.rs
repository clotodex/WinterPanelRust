
use std;
use std::process::*;
use std::process::Stdio;
use std::io::prelude::*;

pub struct PanelInformation {
    pub panel_font: String,
    pub panel_bg: String,
    pub panel_fg: String,
    pub panel_height: u32,
    pub monitors: Vec<MonitorInformation>,
}

pub struct MonitorInformation {
    pub id: u32,
    pub panel_width: u32,
    pub x: u32,
    pub y: u32,
}

pub struct MultiWriter<'a> {
    pub writers: Vec<&'a mut ChildStdin>,
}

impl<'a> Write for MultiWriter<'a> {
    fn write(&mut self, b: &[u8]) -> std::result::Result<usize, std::io::Error> {
        // self.writers.iter().fold(Ok(std::usize::MIN),|r, w| r.and(w.write(b)))
        self.writers
            .iter_mut()
            .map(|ref mut w| w.write(b))
            .fold(Ok(std::usize::MIN), |akk, r| akk.and(r))
    }
    fn flush(&mut self) -> std::result::Result<(), std::io::Error> {
        self.writers.iter_mut().map(|ref mut w| w.flush()).fold(Ok(()), |akk, r| akk.and(r))
    }
}

pub fn setup_panels(info: &PanelInformation) -> Vec<Child> {
    info.monitors
        .iter()
        .map(|monitor| {
            Command::new("/usr/bin/dzen2")
                .arg("-fn")
                .arg(&info.panel_font)
                .arg("-bg")
                .arg(&info.panel_bg)
                .arg("-fg")
                .arg(&info.panel_fg)
                .arg("-w")
                .arg(monitor.panel_width.to_string())
                .arg("-h")
                .arg(info.panel_height.to_string())
                .arg("-x")
                .arg(monitor.x.to_string())
                .arg("-y")
                .arg(monitor.y.to_string())
                .arg("-e")
                .arg("button3=;button4=exec:herbstclient use_index -1;button5=exec:herbstclient \
                      use_index +1")
                .arg("-ta")
                .arg("l")
                .arg("-p")
                .stdin(Stdio::piped())
                .spawn()
                .expect("failed to spawn dzen")
        })
        .collect()
}
