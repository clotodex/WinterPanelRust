use panel::PanelInformation;
use std::collections::HashMap;

pub struct PanelMessage {
    pub left: String,
    pub center: String,
    pub right: String,
}

pub struct PanelWidth {
    pub left: u32,
    pub center: u32,
    pub right: u32,
}

pub struct WPContext {
    pub info: PanelInformation,
    pub pm: PanelMessage, // components: Vec<(&'static str, Vec<&'static str>)>,
    pub pw: PanelWidth,
    pub alias: Vec<(&'static str, Vec<&'static str>)>,
    pub messages: HashMap<String, (String, Option<u32>)>,
}
