use std;
use std::fs::File;
use std::io::prelude::*;
use std::io::BufReader;
use std::process::*;
use std::process::Stdio;

pub fn read_file(path: &str) -> std::io::Result<String> {
    let file = try!(File::open(path));
    let mut buf_reader = BufReader::new(file);
    let mut contents = String::new();
    try!(buf_reader.read_to_string(&mut contents));
    Ok(contents)
}

// herbstclient
pub fn herbstclient(args: Vec<&str>) {
    let ecode = Command::new("/usr/bin/herbstclient")
        .args(args)
        .spawn()
        .expect("failed to spawn herbstclient command")
        .wait()
        .expect("emit_hook did not stop");
    assert!(ecode.success());
}

pub fn textwidth(args: Vec<&str>) -> std::result::Result<String, std::string::FromUtf8Error> {
    let out = Command::new("/usr/bin/textwidth")
        .args(args)
        .output()
        .expect("failed to get monitor rectangle");
    String::from_utf8(out.stdout)
}

pub fn publish_event(elements: Vec<&str>) {
    // replace with a herbstclient call
    let ecode = Command::new("/usr/bin/herbstclient")
        .arg("emit_hook")
        .args(elements)
        .spawn()
        .expect("failed to spawn herbstclient emit hook")
        .wait()
        .expect("emit_hook did not stop");
    assert!(ecode.success());
}

pub fn setup_eventlistener() -> Child {
    Command::new("/usr/bin/herbstclient")
        .arg("--idle")
        .stdout(Stdio::piped())
        .spawn()
        .expect("failed to spawn herbstclient")
}
