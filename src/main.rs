mod panel;
mod system;
mod error;
mod context;

extern crate toml;
extern crate regex;
#[macro_use]
extern crate lazy_static;

use std::process::Child;
use std::io::prelude::*;
use std::io::BufReader;

use toml::Value;
use regex::Captures;
use panel::*;
use system::*;
use error::*;
use helper::*;
use context::*;

mod helper {
    use std;
    use regex::Regex;
    use system::textwidth;
    use std::process::Command;
    use context::*;
    use panel::*;

    pub fn get_monitor_configuration() -> Result<Vec<MonitorInformation>, Box<std::error::Error>> {
        let hlwm_monitors_output = try!(Command::new("/usr/bin/herbstclient")
            .arg("list_monitors")
            .output());
        let hlwm_monitors = try!(String::from_utf8(hlwm_monitors_output.stdout));

        let monitor_ids: Vec<&str> = hlwm_monitors.lines()
            .flat_map(|line| line.split(':').next())
            .collect();

        let geometry_infos = monitor_ids.iter()
            .map(|ref hlwm_monitor_number| {
                Command::new("/usr/bin/herbstclient")
                    .arg("monitor_rect")
                    .arg(hlwm_monitor_number)
                    .output()
                    .expect("failed to get monitor rectangle")
            })
            .flat_map(|out| String::from_utf8(out.stdout));

        let monitors = monitor_ids.iter()
            .zip(geometry_infos)
            .map(|(a, b)| (a.parse().unwrap(), b))
            .map(|(id, hlwm_monitor_geometry)| {
                let mut geometry = hlwm_monitor_geometry.split(' ');
                MonitorInformation {
                    id: id,
                    x: geometry.next()
                        .ok_or("geometry iterator is empty".to_owned())
                        .and_then(|s| {
                            s.parse().map_err(|err: std::num::ParseIntError| err.to_string())
                        })
                        .unwrap(),
                    y: geometry.next().unwrap().parse().unwrap(),
                    panel_width: geometry.next().unwrap().parse().unwrap(),
                }
            })
            .collect();

        Ok(monitors)
    }

    lazy_static! {
    pub static ref EVENT_REGEX: Regex = Regex::new(r"\{(.*?)\}").unwrap();
}

    pub fn filter_out_events(messages: &PanelMessage) -> Vec<String> {
        let mut caps: Vec<String> =
            EVENT_REGEX.captures_iter(&messages.left).map(|cap| cap[1].to_owned()).collect();
        caps.append(&mut EVENT_REGEX.captures_iter(&messages.center)
            .map(|cap| cap[1].to_owned())
            .collect());
        caps.append(&mut EVENT_REGEX.captures_iter(&messages.right)
            .map(|cap| cap[1].to_owned())
            .collect());
        return caps;
    }

    pub fn calculate_width(message: &str, font: &str) -> u32 {
        lazy_static!{
        static ref DZEN_REGEX: Regex = Regex::new(r"\^[^(]*\([^)]*\)").unwrap();
    }
        // TODO support positioning inside of message
        let stripped = DZEN_REGEX.replace_all(message, "").into_owned();
        println!("stripped: {}", stripped);
        let output = textwidth(vec![font, &stripped]).unwrap();
        println!("output: {}", output);
        output.trim().parse().unwrap()

    }

}

// main functionality
fn load_config() -> Result<WPContext, Box<std::error::Error>> {

    // TODO make a macro for field reading

    println!("## COMPONENTS AND EVENTS ###");

    println!("loading file...");
    let content = try!(read_file("winterpanel.toml"));
    println!("parsing config...");
    let config: Value = try!(content.parse());

    // parsing panel messages
    let pm = PanelMessage {
        left: try!(config.get("left")
            .and_then(|o| o.as_str())
            .map(|s| s.to_owned())
            .ok_or(MalformedConfigError::FieldNotExisting("left"))),
        center: try!(config.get("center")
            .and_then(|o| o.as_str())
            .map(|s| s.to_owned())
            .ok_or(MalformedConfigError::FieldNotExisting("center"))),
        right: try!(config.get("right")
            .and_then(|o| o.as_str())
            .map(|s| s.to_owned())
            .ok_or(MalformedConfigError::FieldNotExisting("right"))),
    };

    // parsing panel configuration
    let info = PanelInformation {
        panel_font: try!(config.get("font")
            .and_then(|o| o.as_str())
            .map(|s| s.to_owned())
            .ok_or(MalformedConfigError::FieldNotExisting("font"))),
        panel_bg: try!(config.get("background")
            .and_then(|o| o.as_str())
            .map(|s| s.to_owned())
            .ok_or(MalformedConfigError::FieldNotExisting("background"))),
        panel_fg: try!(config.get("foreground")
            .and_then(|o| o.as_str())
            .map(|s| s.to_owned())
            .ok_or(MalformedConfigError::FieldNotExisting("foreground"))),
        panel_height: try!(config.get("height")
            .and_then(|o| o.as_integer())
            .ok_or(MalformedConfigError::FieldNotExisting("height"))) as u32,
        monitors: get_monitor_configuration().unwrap(),
    };

    let left_stripped = EVENT_REGEX.replace_all(&pm.left, "").into_owned();
    let center_stripped = EVENT_REGEX.replace_all(&pm.center, "").into_owned();
    let right_stripped = EVENT_REGEX.replace_all(&pm.right, "").into_owned();

    let pw = PanelWidth {
        left: calculate_width(&left_stripped, &info.panel_font),
        center: calculate_width(&center_stripped, &info.panel_font),
        right: calculate_width(&right_stripped, &info.panel_font),
    };

    // filtering events and setting default message
    let events = filter_out_events(&pm)
        .iter()
        .map(|e| {
            (e.to_owned() /* TODO WHY?? */, ("loading...".to_string(), None)) //TODO calculate length lazy on update
        })
        .collect();

    // identify aliases
    // read in aliases (alias in config, else it defaults to the file)
    // only reason for aliases is to know what to start
    // what if you could put that in the config? (paths)
    // TODO

    Ok(WPContext {
        info: info,
        pm: pm,
        pw: pw,
        alias: vec![],
        messages: events,
    })
    // TODO start up components
}

// TODO maybe make multithreaded and trigger via channels
fn update_dzen(mut panel: &mut MultiWriter, context: &WPContext) {
    // replace events with their messages
    // calculate each individual width
    // TODO send hook [wp] action componentname hover/click
    // FIXME how to handle overlapping lines?
    // FIXME regard relative and absolute positioning
    // put together and commit to dzen

    let replace_and_count = |message: &str, width: &mut u32| {

        EVENT_REGEX.replace_all(message, |caps: &Captures| {
                let (message, width_op) = context.messages[&caps[1]].clone();
                if let Some(w) = width_op {
                    *width += w;
                } else {
                    *width += calculate_width(&message, &context.info.panel_font);
                }
                message
            })
            .into_owned()

    };

    let mut width_left = context.pw.left.clone();
    let mut width_center = context.pw.center.clone();
    let mut width_right = context.pw.right.clone();

    let real_left = replace_and_count(&context.pm.left, &mut width_left);
    let real_center = replace_and_count(&context.pm.center, &mut width_center);
    let real_right = replace_and_count(&context.pm.right, &mut width_right);

    for m in &context.info.monitors {
        let half_of_total = m.panel_width / 2;

        let center_position = half_of_total - width_center / 2;
        let right_position = m.panel_width - width_right;

        write!(&mut panel,
               "{}^pa({}){}^pa({}){}\n",
               real_left,
               center_position,
               real_center,
               right_position,
               real_right)
            .unwrap();
    }
}


fn handle_events(listener: &mut Child, panel: &mut MultiWriter, context: &mut WPContext) {
    // preparing eventlistener //TODO maybe outsource this to the main function
    let stdout = listener.stdout.as_mut().expect("could not get stdout");
    let reader = BufReader::new(stdout);

    // request an update from every component
    herbstclient(vec!["emit_hook", "[wp]", "reload"]);

    // iterate over all cought events
    for line_result in reader.lines() {

        let line = line_result.unwrap();

        // TODO remove
        //if panel.println(&line).is_err() {
        //    break;
        //}//.expect("oops could not write");

        // a hlwm event's fields are separated by tabs
        let mut parts = line.split("\t");
        match parts.next() {
            // when it is a winterpanel event
            Some("[wp]") => (), //TODO maybe call handle_wp_events instead of handling it sequentially
            // hlwm is reloading -> quit panels
            Some("reload") |
            Some("quit_panel") => break,
            _ => {
                println!("not wp event");
                continue;
            },
        }

        // sent by the panel so just ignore them - maybe redefine protocol to have component channel and panel channel and filter it out by regex
        match parts.next() {
            Some("event")    => {
                    let event_name = parts.next().unwrap();
                    let message = parts.next().unwrap();
                    let width_op = parts.next().and_then(|s| s.parse().ok());

                    context.messages.remove(event_name); //TODO this is weird
                    context.messages.insert(event_name.to_owned(), (message.to_owned(), width_op));

                    update_dzen(panel, context);
                },
                // #parts[2] is event name
				//event="${parts[2]}"
				//
				//#parts[3] is dzen message
				//message="${parts[3]}"
				//
				//#parts[5] is optional: confirm-receive
				//if [ "${parts[5]}x" = "confirmx" ]
				//then
				//	herbstclient emit_hook "[wp]" "confirm" "$event"
				//fi

				//#check if msg is the same as before only on different -- update dzen
				//if [ "${events[$event]}x" != "${message}x" ]
				//then
				//	#parts[4] is textwidth (calculate when not supplied and calculate only when it is really an update
				//	if [ -z ${parts[4]} ]
				//	then
				//		debug "calculating message length"
				//		msg_len[$event]="$(calculate_width "${message}")"
				//	else
				//		debug "taking message length for granted"
				//		msg_len[$event]="${parts[4]}"
				//	fi

				//	debug "update"
				//	events[$event]="${message}"
				//	update_dzen
				//fi
				//;;


                //TODO if reload panel - restart everything if this is supposed to be supported
                //TODO handle request (types: sheduling, ...?)
                //TODO fill timing array & stop and start sheduler
                //sheduler is background task? stop and start for new timings
            Some("request")  => println!("TODO"),
            Some("ping")     => publish_event(vec!("[wp]","pong",parts.next().unwrap())),
            Some("reload")   |
            Some("pong")     |
            Some("confirm")  |
            Some("action")   |
            Some("quit")     => println!("ignored"),
            Some("shutdown") => unimplemented!(),
            Some(x) => println!("Unkown wp event: {}",x),
            None => println!("[wp] has to be followed by something"),
        }
    }
}

// TODO make multi writer parallel
// TODO make multi writer generic
fn main() {
    println!("stopping previos panel(s)...");
    publish_event(vec!["quit_panel"]);
    println!("loading configuration");
    let mut context = load_config().unwrap();
    println!("starting eventlistener...");
    let mut listener = setup_eventlistener();
    println!("starting panel...");
    for m in &context.info.monitors {
        herbstclient(vec!["pad", &m.id.to_string(), &context.info.panel_height.to_string()]);
    }
    let mut panels = setup_panels(&context.info);
    {
        let mut writer = MultiWriter {
            writers: panels.iter_mut()
                .map(|panel| panel.stdin.as_mut().expect("could not open stdin"))
                .collect(),
        };

        println!("handling events now:");
        handle_events(&mut listener, &mut writer, &mut context);

        println!("Done reading");
        publish_event(vec!["[wp]", "quit", "*"]);
        if listener.kill().is_err() {
            println!("the listener seems to be already dead");
        }
        listener.wait().expect("failed to wait on listener");

    }
    panels.iter_mut()
        .map(|ref mut c| c.kill().and(c.wait()))
        .fold(Ok(()), |akk, r| r.and(akk)) //TODO replace with collect
        .expect("panels did not shutdown");

    println!("shutdown!");
}
