use std;

#[derive(Debug)]
pub enum MalformedConfigError {
    FieldNotExisting(&'static str),
    FieldInWrongFormat(String, &'static str),
}

impl std::fmt::Display for MalformedConfigError {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        match *self {
            // Both underlying errors already impl `Display`, so we defer to
            // their implementations.
            MalformedConfigError::FieldNotExisting(ref field) => {
                write!(f, "Field not exisiting: {}", field)
            }
            MalformedConfigError::FieldInWrongFormat(ref field, ref _type) => {
                write!(f,
                       "Field is not in proper format: could not parse {} to {}",
                       field,
                       _type)
            }

        }
    }
}

impl std::error::Error for MalformedConfigError {
    fn description(&self) -> &str {
        match *self {
            MalformedConfigError::FieldNotExisting(ref field) => field,
            MalformedConfigError::FieldInWrongFormat(ref field, ref _type) => field,
        }
    }

    fn cause(&self) -> Option<&std::error::Error> {
        None
    }
}

impl From<&'static str> for MalformedConfigError {
    fn from(err: &'static str) -> MalformedConfigError {
        // FIXME parse FieldInWrongFormat error roo
        MalformedConfigError::FieldNotExisting(err)
    }
}
