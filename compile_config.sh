#!/bin/bash

## COLORS ##

bg="#555555"
fg="#ffffff"

## ICONS ##
clock="^i(icons/clock.xbm)"

## SEPARATOR ##
sep="|"

{
	echo "#[[message]]"
	echo "left=\"{title} ${sep}\""
	echo "center=\"{tags}\""
	echo "right=\"{storage} {cpu} {mem} {network} {volume} {screensaver} ${sep} {battery} ${sep} ${clock} {time} ${sep} {date}\""
	echo "panel_height=18"
	echo ""
	echo "#[[panel]]"
	echo "font=\"-*-fixed-medium-*-*-*-14-*-*-*-*-*-*-*\""
	echo "background=\"${bg}\""
	echo "foreground=\"${fg}\""
	echo "height=18"
	echo ""
	echo "#[[aliases]]"
	echo "time=\"components/time_and_date.sh\""
	echo "date=\"components/time_and_date.sh\""
	echo "title=\"components/title.sh\""
	echo "battery=\"components/battery.sh\""
} > winterpanel.toml
