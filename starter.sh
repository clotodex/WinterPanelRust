#!/bin/bash

./components/wrapper.sh &
wrapper="$!"
./components/title.sh &
title="$!"
./components/tags.sh &
tags="$!"
./components/scheduler.sh &
scheduler="$!"
cargo run &
panel="$!"

sleep 10
kill $wrapper $title $tags $scheduler
herbstclient emit_hook quit_panel
sleep 2
kill $panel
